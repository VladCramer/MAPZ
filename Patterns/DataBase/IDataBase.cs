﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.DataBase 
{
	public interface IDataBase
	{
		public UserData Request();
	}
}
