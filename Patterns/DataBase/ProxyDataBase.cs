﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.DataBase
{
	class ProxyDataBase : IDataBase
	{
		private RealDataBase dataBase;

		public ProxyDataBase(RealDataBase dataBase)
		{
			this.dataBase = dataBase;
		}

		public UserData Request()
		{
			if (CheckData())
			{
				UserData userData = dataBase.Request();

				if (userData != null)
				{
					Console.WriteLine("Login success!\n");
					return userData;
				}
				else
				{
					Console.WriteLine("User does not exist!");
					Console.ReadKey();
					return null;
				}
			}
			else
			{
				Console.WriteLine("Invalid user data!");
				Console.ReadKey();
				return null;
			}
		}

		private bool CheckData() 
		{
			if (dataBase == null || dataBase.login.Length > 20 || dataBase.password.Length > 20)
			{
				return false;
			}

			return true;
		}
	}
}
