﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Project;

namespace Patterns.DataBase
{
	public class RealDataBase : IDataBase
	{
		public string login { get; set; }
		public string password { get; set; }

		public RealDataBase(string login, string password)
		{
			this.login = login;
			this.password = password;
		}

		public UserData Request()
		{
			string existingName = "name";
			string existingPassword = "pass";
			DefaultProject[] projectList = {
				new DefaultProject("exProject", new List<User.Task>
				{
					new User.Task("firstTask", "..."),
					new User.Task("secondTask", "...")
				})
			};

			if (login == existingName && password == existingPassword)
			{
				return new UserData
				{
					userName = existingName,
					userPassword = existingPassword,
					userProjectList = projectList
				};
			}

			return null;
		}
	}
}
