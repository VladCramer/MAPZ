﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.ProjectAnalysis;

namespace Patterns
{
	public class Facade: INotifier
	{
		public UserData user { get; set; }

		public IWorkspaceCreator workspaceCreator = null;
		public IWorkArea workArea = null;
		public IMenu menu = null;
		public List<INotifiableObject> subList = new List<INotifiableObject>();

		public bool Authorize()
		{
			string inputName;
			string inputPassword;

			Console.Write("Enter name: ");
			inputName = Console.ReadLine();
			Console.Write("Enter password: ");
			inputPassword = Console.ReadLine();
			Console.Clear();

			UserAuthorizer userAuthorizer = UserAuthorizer.Instance;

			if (!userAuthorizer.SignIn(inputName, inputPassword))
			{
				return false;
			}

			this.user = userAuthorizer.GetUserData();

			return true;
		}

		public bool SelectDevice()
		{
			IWorkspaceCreator[] WorkspaceCreators = new IWorkspaceCreator[] {
				new WorkspaceCreators.DesktopWorkspaceCreator(),
				new WorkspaceCreators.MobileWorkspaceCreator(),
				new WorkspaceCreators.WebWorkspaceCreator()
			};

			Console.WriteLine("Your device?");

			for (int i = 0; i < WorkspaceCreators.Length; i++)
			{
				Console.WriteLine($"{i + 1}. {WorkspaceCreators[i]}");
			}

			Console.Write("Enter: ");
			string inputDevice = Console.ReadLine();
			Console.Clear();

			if (!int.TryParse(inputDevice, out int inputDeviceIndex))
			{
				Console.WriteLine("It's not a number!");
				Console.ReadKey();
				return false;
			}

			if (inputDeviceIndex > WorkspaceCreators.Length || inputDeviceIndex < 1)
			{
				Console.WriteLine("A device with this number does not exist!");
				Console.ReadKey();
				return false;
			}

			inputDeviceIndex--;

			workspaceCreator = WorkspaceCreators[inputDeviceIndex];

			return true;
		}

		public bool ShowWorkspace()
		{
			workArea = this.workspaceCreator.CreateWorkArea();

			menu = this.workspaceCreator.CreateMenu();

			Console.WriteLine($"Width: {workArea.GetWidth()} Heigth: {workArea.GetHeight()}");

			Console.WriteLine(workArea.Show());

			Console.WriteLine("\nMenu:");

			for (int i = 0; i < menu.GetMenuItems().Length; i++)
			{
				Console.WriteLine($"{i + 1}. {menu.GetMenuItems()[i]}");
			}

			Console.Write("Enter: ");
			string inputMenuItem = Console.ReadLine();
			Console.Clear();

			if (!int.TryParse(inputMenuItem, out int inputMenuItemIndex))
			{
				Console.WriteLine("It's not a number!");
				Console.ReadKey();
				return false;
			}

			inputMenuItemIndex--;

			menu.SelectMenuItem(inputMenuItemIndex);

			Console.WriteLine("Analysis y/n ?");
			string withAnalysisString = Console.ReadLine();

			if (withAnalysisString == "y")
			{
				Console.WriteLine("Select method:");
				Console.WriteLine("1.SWOT");
				Console.WriteLine("2.Decart method");
				Console.Write("Enter: ");

				string inputMethodNumber = Console.ReadLine();

				if (!int.TryParse(inputMethodNumber, out int inputMethodIndex))
				{
					Console.WriteLine("It's not a number!");
					Console.ReadKey();
					return false;
				}

				inputMethodIndex--;

				if (inputMethodIndex == 0)
				{
					Console.WriteLine(new ProjectAnalyzer(new SWOTMethod()).Analyze());
				}
				else if (inputMethodIndex == 1)
				{
					Console.WriteLine(new ProjectAnalyzer(new DecartMethod()).Analyze());
				}

				AddNotifiableObject(menu.projects.Last());

				Notify();
			}

			return true;
		}

		public void AddNotifiableObject(INotifiableObject notifiableObject)
		{
			subList.Add(notifiableObject);
		}

		public void RemoveNotifiableObject(INotifiableObject notifiableObject)
		{
			subList.Remove(notifiableObject);
		}

		public void Notify()
		{
			Console.WriteLine("Sending notifications...");

			foreach (var sub in subList)
			{
				sub.Update(this);
			}
		}
	}
}
