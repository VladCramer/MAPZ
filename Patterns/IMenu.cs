﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Project;

namespace Patterns {
	public abstract class IMenu {

		public List<Project.Project> projects = new List<Project.Project>();

		public abstract string[] GetMenuItems();

		public abstract void SelectMenuItem(int itemIndex);

		protected string CreateProject() {
			bool withAnalysis;
			Console.WriteLine("With analysis y/n ?");
			string withAnalysisString = Console.ReadLine();

			if (withAnalysisString == "y")
			{
				withAnalysis = true;
			}
			else
			{
				withAnalysis = false;
			}

			Console.Write("Enter project title: ");
			string projectTitle = Console.ReadLine();

			Console.Write("Enter task title: ");
			string firstTaskTitle = Console.ReadLine();

			Console.Write($"{firstTaskTitle} text: ");
			string firstTaskText = Console.ReadLine();

			Console.Write("Enter task title: ");
			string secondTaskTitle = Console.ReadLine();

			Console.Write($"{secondTaskTitle} text: ");
			string secondTaskText = Console.ReadLine();

			Console.Clear();

			if (withAnalysis == true)
			{

				projects.Add(new ProjectDecoratorSWOT(new DefaultProject(projectTitle, new List<User.Task> {
				new User.Task(firstTaskTitle, firstTaskText),
				new User.Task(secondTaskTitle, secondTaskText)
				})));

				return projects.Last().Show();
			}
			else
			{
				projects.Add(new DefaultProject(projectTitle, new List<User.Task> {
				new User.Task(firstTaskTitle, firstTaskText),
				new User.Task(secondTaskTitle, secondTaskText)
				}));

				return projects.Last().Show();
			}
		}
	}
}
