﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
	public interface INotifier
	{
		public void AddNotifiableObject(INotifiableObject notifiableObject);

		public void RemoveNotifiableObject(INotifiableObject notifiableObject);

		public void Notify();
	}
}
