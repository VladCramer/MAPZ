﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns {
	public interface IWorkArea {
		public string Show();
		public double GetWidth();
		public double GetHeight();
	}
}
