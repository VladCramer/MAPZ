﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Menu {
	class DesktopMenu: IMenu {
		private string[] MenuItems;

		public DesktopMenu() {
			MenuItems = new string[] {
				"View project list",
				"Create project",
				"Analyze project",
				"View active devices",
				"Exit"
			};
		}
		public override string[] GetMenuItems() {
			return this.MenuItems;
		}

		public override void SelectMenuItem(int itemIndex) {
			if (itemIndex == 1) {
				Console.WriteLine(CreateProject());
			}
			else {
				Console.WriteLine("Page missing");
			}
		}
	}
}
