﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Menu {
	class MobileMenu : IMenu {
		private string[] MenuItems;

		public MobileMenu() {
			MenuItems = new string[] { 
				"Create project",
				"View project list",
				"Analyze project"
			};
		}

		public override string[] GetMenuItems() {
			return this.MenuItems;
		}

		public override void SelectMenuItem(int itemIndex) {
			if (itemIndex == 0) {
				Console.WriteLine(CreateProject());
			}
			else {
				Console.WriteLine("Page missing");
			}
		}
	}
}
