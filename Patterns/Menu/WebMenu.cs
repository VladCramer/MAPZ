﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Menu {
	class WebMenu: IMenu {
		private string[] MenuItems;

		public WebMenu() {
			MenuItems = new string[] {
				"Create project",
				"View project list",
				"Analyze project",
				"View active devices",
				"Change profile"
			};
		}
		public override string[] GetMenuItems() {
			return this.MenuItems;
		}

		public override void SelectMenuItem(int itemIndex) {
			if (itemIndex == 0) {
				Console.WriteLine(CreateProject());
			}
			else {
				Console.WriteLine("Page missing");
			}
		}
	}
}
