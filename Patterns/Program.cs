﻿using System;
using System.Collections.Generic;

namespace Patterns {
	class Program 
	{
		static void Main(string[] args) {

			Facade mainFacade = new Facade();

			if (!mainFacade.Authorize()) return;

			if (!mainFacade.SelectDevice()) return;

			if (!mainFacade.ShowWorkspace()) return;

			Console.ReadKey();
		}
	}
}
