﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Project
{
	abstract class AProjectDecorator: Project
	{
		private Project project;

		public AProjectDecorator(Project project)
		{
			this.project = project;
		}

		public void SetProject(Project project)
		{
			this.project = project;
		}

		public override string Show()
		{
			if (project != null)
			{
				return project.Show();
			}
			else
			{
				return "Project does not exist!";
			}
		}
	}
}
