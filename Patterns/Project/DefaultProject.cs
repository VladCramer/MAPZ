﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Patterns.User;

namespace Patterns.Project 
{
	public class DefaultProject: Project
	{
		public string title = "";

		public List<Task> taskList { get; private set; } = new List<Task>();

		public DefaultProject(string title, List<Task> taskList)
		{
			this.title = title;
			this.taskList = taskList;
		}

		public void AddTask(string title, string text = "") {
			taskList.Add(new Task(title, text));
		}

		public void SetTitle(string title) {
			this.title = title;
		}

		public override string Show() {
			string result = $"Project - {this.title}\n";
			result += "\nTask List:\n";

			for(int i = 0; i < taskList.Count; i++) {
				result += $"{i + 1}. {taskList[i]}\n";
			}

			return result;
		}
	}
}
