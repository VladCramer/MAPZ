﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.WorkspaceCreators;

namespace Patterns.Project
{
	public abstract class Project: INotifiableObject
	{
		public void Update(INotifier notifier)
		{
			Facade facade = notifier as Facade;
			if (facade != null)
			{
				if (facade.workspaceCreator is DesktopWorkspaceCreator)
				{
					Console.WriteLine("Desktop notify!");
				}
				else if (facade.workspaceCreator is MobileWorkspaceCreator)
				{
					Console.WriteLine("Mobile notify!");
				}
				else
				{
					Console.WriteLine("Web notify!");
				}
			}
			else
			{
				Console.WriteLine("Incorrect notifier!");
			}
		}

		public abstract string Show();
	}
}
