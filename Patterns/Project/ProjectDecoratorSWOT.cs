﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Project
{
	class ProjectDecoratorSWOT : AProjectDecorator
	{
		public ProjectDecoratorSWOT(Project project) : base(project) { }

		public override string Show()
		{
			return $"SWOT analysis...\n{base.Show()}\n\nSWOT result!";
		}
	}
}
