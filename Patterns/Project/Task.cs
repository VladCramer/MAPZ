﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.User
{
	public class Task
	{
		public string title { get; set; }

		public string text { get; private set; }

		public Task(string title, string text = "")
		{
			this.title = title;
			this.text = text;
		}

		public override string ToString()
		{
			return $"{title}: {text}";
		}
	}
}
