﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.ProjectAnalysis
{
	public abstract class AnalysisMethod
	{
		public string Analyze()
		{
			string result = "";
			result += CollectProjectInformation();
			result += Hook();
			result += Grading();
			result += ProcessingResult();

			return result;
		}

		protected string Hook() => "";

		protected string CollectProjectInformation()
		{
			return "All information collected.\n";
		}

		protected abstract string Grading();

		protected string ProcessingResult()
		{
			return "Result ready.\n";
		}
	}
}
