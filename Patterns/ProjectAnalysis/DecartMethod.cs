﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.ProjectAnalysis
{
	public class DecartMethod : AnalysisMethod
	{
		protected override string Grading()
		{
			return "Decart grading.\n";
		}
	}
}
