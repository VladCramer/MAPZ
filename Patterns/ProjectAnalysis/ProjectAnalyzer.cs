﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.ProjectAnalysis
{
	public class ProjectAnalyzer
	{
		private AnalysisMethod analysisMethod;

		public ProjectAnalyzer(AnalysisMethod analysisMethod)
		{
			this.analysisMethod = analysisMethod;
		}

		public void SetAnalysisMethod(AnalysisMethod analysisMethod)
		{
			this.analysisMethod = analysisMethod;
		}

		public string Analyze()
		{
			return analysisMethod.Analyze();
		}
	}
}
