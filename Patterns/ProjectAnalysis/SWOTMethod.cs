﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.ProjectAnalysis
{
	public class SWOTMethod: AnalysisMethod
	{
		protected override string Grading()
		{
			return "SWOT grading.\n";
		}
	}
}
