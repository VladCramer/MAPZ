﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.User;
using Patterns.DataBase;

namespace Patterns {
	public class UserAuthorizer {

		private static readonly Lazy<UserAuthorizer> _instance = new Lazy<UserAuthorizer>(() => new UserAuthorizer());

		public static UserAuthorizer Instance { get { return _instance.Value; } }

		private bool authorizationStatus = false;

		private UserData userData;

		IDataBase dataBase;

		private UserAuthorizer() { }

		public bool SignIn(string userName, string userPassword) {
			dataBase = new ProxyDataBase(new RealDataBase(userName, userPassword));

			this.userData = dataBase.Request();

			if (userData != null)
			{
				this.authorizationStatus = true;
				return true;
			}
			else
			{
				this.authorizationStatus = false;
				return false;
			}
		}

		public bool LogOut() {
			userData.userName = string.Empty;
			userData.userPassword = string.Empty;
			userData.userProjectList = null;
			this.authorizationStatus = false;
			return true;
		}

		public UserData GetUserData() {
			return userData;
		}

		public bool IsAuthorized() {
			return authorizationStatus;
		}
	}
}
