﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Project;

namespace Patterns {
	public class UserData {
		public string userName { get; set; }

		public string userPassword { get; set; }

		public DefaultProject[] userProjectList { get; set; }
	}
}
