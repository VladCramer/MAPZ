﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.WorkArea {
	class DesktopWorkArea: IWorkArea {
		private double Width;
		private double Heigth;

		public DesktopWorkArea() {
			this.Width = 1920;
			this.Heigth = 1080;
		}
		public double GetHeight() {
			return Heigth;
		}

		public double GetWidth() {
			return Width;
		}

		public string Show() {
			return "Welcome to the Desktop version.";
		}
	}
}
