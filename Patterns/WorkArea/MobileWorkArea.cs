﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.WorkArea {
	class MobileWorkArea: IWorkArea {
		private double Width;
		private double Heigth;

		public MobileWorkArea() {
			this.Width = 1334;
			this.Heigth = 750;
		}
		public double GetHeight() {
			return Heigth;
		}

		public double GetWidth() {
			return Width;
		}

		public string Show() {
			return "Welcome to the Mobile version.";
		}
	}
}
