﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.WorkArea {
	class WebWorkArea: IWorkArea {
		private double Width;
		private double Heigth;

		public WebWorkArea() {
			this.Width = 1800;
			this.Heigth = 1000;
		}
		public double GetHeight() {
			return Heigth;
		}

		public double GetWidth() {
			return Width;
		}

		public string Show() {
			return "Welcome to the Web version.";
		}
	}
}
