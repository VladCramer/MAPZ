﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.WorkspaceCreators {
	class DesktopWorkspaceCreator: IWorkspaceCreator {
		public IMenu CreateMenu() {
			return new Menu.DesktopMenu();
		}

		public IWorkArea CreateWorkArea() {
			return new WorkArea.DesktopWorkArea();
		}

		public override string ToString() {
			return "Desktop";
		}
	}
}
