﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.WorkspaceCreators {
	class MobileWorkspaceCreator: IWorkspaceCreator {
		public IMenu CreateMenu() {
			return new Menu.MobileMenu();
		}

		public IWorkArea CreateWorkArea() {
			return new WorkArea.MobileWorkArea();
		}

		public override string ToString() {
			return "Mobile";
		}
	}
}
