﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.WorkspaceCreators {
	class WebWorkspaceCreator: IWorkspaceCreator {
		public IMenu CreateMenu() {
			return new Menu.WebMenu();
		}

		public IWorkArea CreateWorkArea() {
			return new WorkArea.WebWorkArea();
		}

		public override string ToString() {
			return "Web Browser";
		}
	}
}
